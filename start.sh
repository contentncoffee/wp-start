#!/bin/bash

read -p "Are you sure that you are in the directory you want to be in to start a wordpress project? " -n 1 -r
echo
if [[ ! $REPLY =~ ^[Yy]$ ]]
then
    echo "Navigate to the directory that you want to start in and try again."
    [[ "$0" = "$BASH_SOURCE" ]] && exit 1 || return 1
fi


read -p "Do you have a local empty database for this project? " -n 1 -r
echo
if [[ ! $REPLY =~ ^[Yy]$ ]]
then
    echo "Create an empty local dev database for this project and try again."
    [[ "$0" = "$BASH_SOURCE" ]] && exit 1 || return 1
fi


read -p "What is the name of this database? " -r
echo
DATABASE=$REPLY


read -p "Do you have a local hostname for this project? " -n 1 -r
echo
if [[ ! $REPLY =~ ^[Yy]$ ]]
then
    echo "Create a local hostname for this project and try again."
    [[ "$0" = "$BASH_SOURCE" ]] && exit 1 || return 1
fi


read -p "What is the local hostname of this project? " -r
echo
HOSTNAME=$REPLY


# First delete everything in the directory
rm -rf ./*

# OK go get the repo as a zipo.
wget -q https://bitbucket.org/contentncoffee/wp-start/get/master.zip
unzip master.zip
rm -rf master.zip
dd=$(ls | sort -n | head -1)
echo $dd
mv $dd/* ./
rm -rf $dd
rm start.sh
rm README.md

# isntall grumpers
composer install

# make the project theme
cd wp-content/themes/project
npm install
npm run build
cd -

#now the wordpress
wp core download
cp wp-config-sample.php wp-config.php
wp config set DB_NAME $DATABASE
wp config set DB_USER root
wp config set DB_PASSWORD root
wp config set DB_HOST 127.0.0.1
wp config shuffle-salts
wp config set WP_DEBUG true --raw
wp config set WP_HOME https://$HOSTNAME
wp config set WP_SITEURL https://$HOSTNAME
wp config set table_prefix $DATABASE\_


wp db reset --yes
wp core install --url=http://$HOSTNAME --title=$DATABASE --admin_user=$DATABASE --admin_email=$DATABASE@$DATABASE.com

# Install plugins
sh wp-content/plugins/plugins.sh

wp plugin activate ccfigs
wp plugin activate ccforms
wp plugin activate project

# Activate the theme
wp theme activate project

# Timezone
wp option update timezone_string Europe/Brussels
# Files folders
wp option set uploads_use_yearmonth_folders 0


# Image Sizes

wp option set medium_large_size_w 0
wp option set medium_large_size_h 0
wp option set large_size_h 0
wp option set large_size_w 0
wp option set medium_size_w 0
wp option set medium_size_h 0
wp option set thumbnail_size_h 150
wp option set thumbnail_size_w 150
wp option set thumbnail_crop 1

wp rewrite structure '/%postname%/'
wp rewrite flush

wp user update 1 --user_pass=$DATABASE --display_name=$DATABASE --role='administrator'