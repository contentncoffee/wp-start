<?php

//$seconds_to_cache = 300;
//$ts = gmdate("D, d M Y H:i:s", time() + $seconds_to_cache) . " GMT";
//header("Expires: $ts");
//header("Pragma: cache");
//header("Cache-Control: max-age=$seconds_to_cache");
get_header();

if (have_posts()) {
    // Load posts loop.
    while (have_posts()) {
        the_post();
        $event = get_post();
        $event_details_raw = get_fields($event->ID, false);
        $event_details = get_fields($event->ID);
        $attachments = ['attachment_1', 'attachment_2', 'attachment_3'];
        $encodedAddress = urlencode($event_details['location_address_1'] . $event_details['location_address_2']);
        ?>

      <div class="container-md main-content single-event mb-4">
          <div class="row">
              <div class="col-lg-12 text-center">
                  <h1><?= the_title() ?></h1>
              </div>
          </div>
        <div class="row no-gutters">
          <div class="col-lg-12">
              <?php the_content(); ?>
          </div>
          <div class="col-12">
              <div class="container-md">
                  <div class="row">
                      <div class="registration mt-2 mb-4 col-12">
                          <?php
                          if ($event_details['allow_registrations'] && (empty($event_details_raw['registrations_starting_date']) || strtotime($event_details_raw['registrations_starting_date']) <= time()) && (empty($event_details_raw['registrations_ending_date']) || strtotime($event_details_raw['registrations_ending_date']) >= time())) {
                              ?>
                              <button class="btn btn-primary" id="event-register-button" type="button">
                                  Register to this event
                              </button>
                              <?php
                          }
                          ?>
                      </div>
                  </div>
              </div>
          </div>
            <?php if (is_array($event_details['attachment_1'])): ?>
                <div class="col-12 attachments mb-4">
                    <div class="container-md">
                        <h3>Related <span>attachments:</span></h3>
                        <?php
                        foreach ($attachments as $attachment) {
                            if (is_array($event_details[$attachment]) === false) {
                                continue;
                            }
                            ?>
                            <a href="<?= $event_details[$attachment]['url'] ?>" target="_blank"><i class="far fa-file-pdf"></i> <?= $event_details[$attachment]['title'] ?></a>
                            <?php
                        }
                        ?>
                    </div>
                </div>
            <?php endif; ?>
        </div>
          <div class="container-md">
          <div class="row mb-4">
              <?php if (isset($event_details['sponsored_by_logo'])): ?>
                  <div class="<?= is_array($event_details['attachment_1']) ? 'col-lg-6 col-12' : 'col-12' ?> sponsor" >
                      <h3>Sponsored <span>by:</span></h3>
                      <a href="<?= $event_details['sponsored_by_url']; ?>" target="_blank">
                          <?php
                            if ($event_details['sponsored_by_logo']):
                          ?>
                                <div class="sponsor-logo"><img src="<?= $event_details['sponsored_by_logo']; ?>" alt=""></div>
                          <?php
                            else:
                          ?>
                                <div><h3><?= $event_details['sponsored_by_name']; ?></h3></div>
                          <?php
                            endif;
                          ?>
                      </a>
                  </div>
              <?php endif; ?>

          </div>
            <div class="row event-details">
                <div class="col-12 col-lg-6 mb-3 mb-lg-0 map">
                    <iframe
                            frameborder="0" style="border:0"
                            src="https://www.google.com/maps/embed/v1/place?key=<?= ccfigValue('google-maps-api-key') ?>&q=<?= $encodedAddress ?>" allowfullscreen>
                    </iframe>
                </div>
                <div class="col-12 col-lg-3">
                    <div class="date-wrapper mb-2">
                        <div class="date-header"><i class="far fa-calendar-alt pr-lg-2"></i> <b>Date</b></div>
                        <div class="date-information">
                            <?= $event_details['starting_date'] ?> <?= $event_details['starting_time'] ?>
                            <?php if ($event_details['ending_date'] != $event_details['starting_date']) : ?>
                                - <?= $event_details['ending_date'] ?>
                            <?php endif ?>
                        </div>
                    </div>
                    <div class="time-wrapper mb-2">
                        <div class="time-header">
                            <i class="far fa-clock pr-lg-2"></i> <b>Time</b>
                        </div>
                        <div class="time-information">
                            <?= $event_details['starting_time'] ?>
                        </div>
                    </div>
                    <div class="location-wrapper mb-2">
                        <div class="location-header"><i class="fas fa-map-marker-alt pr-lg-2"></i> <b>Location</b></div>
                        <div class="location-name"><?= $event_details['location_name'] ?></div>
                        <div class="location-1"><?= $event_details['location_address_1'] ?></div>
                        <div class="location-2"><?= $event_details['location_address_2'] ?></div>
                    </div>
                </div>
            </div>
              <div class="row register mt-3">
                <div class="col-12">
                    <div class="card card-body">
                        <?php
                        require get_template_directory() . '/forms/event-registration-form.php';
                        ?>
                    </div>
                </div>
              </div>
          </div>
      </div>
        <?php
    }
} else {
    get_404_template();
}

get_footer();
