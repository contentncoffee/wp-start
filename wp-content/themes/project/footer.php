<?php use Project\BootstrapNavMenuWalker;

if (is_privacy_policy()) { ?>
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <!-- OneTrust Cookies List start -->
                <div id="ot-sdk-cookie-policy"></div>
                <!-- OneTrust Cookies List end -->
            </div>
        </div>
    </div>
<?php } ?>

</main>
<footer>
    <div class="footer-wrapper">
        <div class="row footer-menu text-center">
            <div class="col-12 legal-footer-links">
                <?php $companyOutput = wp_nav_menu([
                  'theme_location' => 'company-footer-menu',
                  'before' => '<div class="company-links inline">',
                  'after' => '</div>',
                  'container_class' => '',
                  'items_wrap' => '%3$s',
                  'menu_id' => false,
                  'echo' => false,
                ]);
                echo strip_tags($companyOutput, '<a> <div> <p> <span>');
?>
            </div>
            <div id="footer-nav-contact" class="col-12 mb-3">
                <a href="mailto:<?php pll_e('contact_email'); ?>">
                    <i class="far fa-envelope"></i> <?php pll_e('contact_email'); ?>
                </a>
            </div>
        </div>

        <div class="row footer-company text-center pb-4">
                    <div class="col-12 copyright">
                        A PROJECT &copy; <?php echo date('Y'); ?>&nbsp;All rights reserved
                    </div>
                </div>
            </div>
</footer>
<?php wp_footer(); ?>
</body>
</html>
