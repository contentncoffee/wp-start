<?php

$seconds_to_cache = 300;
$ts = gmdate("D, d M Y H:i:s", time() + $seconds_to_cache) . " GMT";
header("Expires: $ts");
header("Pragma: cache");
header("Cache-Control: max-age=$seconds_to_cache");
get_header();
?>
<div class="container">
    <div class="row">
        <div class="col-lg-12">
            <h1>Oops, not found!</h1>
            <p>
                The url or link you have followed is not handled.
            </p>
            <p>
                Apologies for this, we'll bring you to the homepage in 10 seconds...
            </p>
            <p>
                <a href="/" class="btn btn-primary">GO TO HOMEPAGE</a>
            </p>
            <script>
                setTimeout(function(){
                  document.location.href = '/';
                }, 10000);
            </script>
        </div>
    </div>
</div>
<?php
get_footer();
