<?php
/**
 * @file In this file we register the lazyblocks that are to be used.
 */


/**
 * Bring in all the lazy locks that we have.
 */
function project_load_my_lazy_blocks()
{
    if (function_exists('lazyblocks')) {
        $blocks = [];

      // Why don't we just do a glob and then include?
      // Well globs cost time and we can solve this by just not being lazy.
      // Irony...

      // Create blocks in the lazy blocks directory,
      // Then add a line here.
        $blocks[] = 'heading-1';
        $blocks[] = 'section';
        $blocks[] = 'content';
        $blocks[] = 'space';
        $blocks[] = 'separator';
        $blocks[] = 'cta';
        $blocks[] = 'cards';
        $blocks[] = 'image';
        $blocks[] = 'forms';
        $blocks[] = 'svg';
        $blocks[] = 'image-content';
        $blocks[] = 'content-image';
        $blocks[] = 'two-content';
        $blocks[] = 'twitter-wall';
        $blocks[] = 'events';
        $blocks[] = 'meps';
        $blocks[] = 'news';
        $blocks[] = 'partners';
        $blocks[] = 'secretariat';
        $blocks[] = 'latest-news-events';
        $blocks[] = 'subscribe-form';

      // Let's ROCK!
        foreach ($blocks as $slug) {
          // Each block that we make will not have a wrapper.
          // We don't want to get into an endless DIV game in the html.
          // Plus we want to have control of the class name.
            add_filter('lazyblock/' . $slug . '/allow_wrapper', function ($a, $b) {
                return false;
            }, 10, 2);
          // Require the lazyblock, block code.
            require_once __DIR__ . '/../lazy-blocks/' . $slug . '/' . $slug . '-block.php';
        }
    }
}

// This calls the function, so leave it.
add_action('after_setup_theme', 'project_load_my_lazy_blocks');



/**
 * Only allow the lazyblocks. We do not want uknown blocks littering our project.
 *
 * @param $allowed_blocks
 *
 * @return array
 */
function allowed_block_types($allowed_blocks)
{
    $allowed_blocks = [];
  //$allowed_blocks[] = 'core/paragraph';

    $block_types = WP_Block_Type_Registry::get_instance()->get_all_registered();
    foreach ($block_types as $name => $block_type) {
        if (preg_match("/^lazyblock/", $name)) {
            $allowed_blocks[] = $name;
        }
    }

    return $allowed_blocks;
}

// This calls the function, so leave it.
add_filter('allowed_block_types', 'allowed_block_types', 100);
