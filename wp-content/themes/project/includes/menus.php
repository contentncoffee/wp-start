<?php
/**
 * @file Here we do things with menus. Register them, and massage them.
 */

/**
 * Register the menus for this project.
 */
function project_register_my_menus()
{
    register_nav_menu('main-menu', 'Main Menu');
    register_nav_menu('footer-menu', 'Footer Menu');
    register_nav_menu('company-footer-menu', 'Company Footer Menu');
}
add_action('init', 'project_register_my_menus');


/**
 * Add the bootstrap for bootstrap lis menus
 *
 * @param $classes
 * @param $item
 * @param $args
 *
 * @return array
 */
function project_add_additional_class_on_li($classes, $item, $args)
{
    if (isset($args->bootstrap) && $args->bootstrap) {
        $classes[] = 'nav-item';
        if (in_array('current-menu-item', $classes)) {
            $classes[] = 'active';
        }
    }

    return $classes;
}
add_filter('nav_menu_css_class', 'project_add_additional_class_on_li', 10, 3);

/**
 * Add bootstrapping on the a tags in menus
 *
 * @param $atts
 * @param $item
 * @param $args
 *
 * @return mixed
 */
function project_add_additional_class_on_li_a($atts, $item, $args)
{
    if (isset($args->bootstrap) && $args->bootstrap) {
        $atts['class'] = 'nav-link';
    }

    return $atts;
}
add_filter('nav_menu_link_attributes', 'project_add_additional_class_on_li_a', 10, 3);
