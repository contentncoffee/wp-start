<div class="container-md">
    <div class="row lazyblock-heading-1">
        <div class="col-lg-12 text-{{alignment}}">
            <h1>{{{text}}}</h1>
        </div>
    </div>
</div>