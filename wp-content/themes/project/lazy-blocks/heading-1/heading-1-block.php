<?php

$blockslug = 'heading-1';

// Start the block
$block             = [];
$block['title']    = 'Heading 1';
$block['icon']     = 'dashicons dashicons-buddicons-tracking';
$block['slug']     = 'lazyblock/' . $blockslug;
$block['category'] = 'common';

// Supports
$supports                    = [];
$supports['align']           = [];
$supports['customClassName'] = false;


// Add the support in.
$block['supports'] = $supports;

// Holder for the controls
$controls = [];

// Text
$control                         = [];
$control['label']                = '';
$control['name']                 = 'text';
$control['type']                 = 'text';
$control['child_of']             = '';

// Make an id.
$control_id = 'control-heading-1-text';

// Add the control to the controls
$controls[$control_id] = $control;

// Alignment
$control              = [];
$control['label']     = 'Alignment';
$control['name']      = 'alignment';
$control['type']      = 'radio';
$control['placement'] = 'inspector';
$control['required']  = true;
$control['default']   = 'left';
$control['child_of']  = '';
$control['choices']   = [
    [
        'label' => 'Left',
        'value' => 'left',
    ],
    [
        'label' => 'Center',
        'value' => 'center',
    ],
];

// Make an id.
$control_id = 'control-heading-1-alignment';

// Add the control to the controls
$controls[$control_id] = $control;


// Add the controls to the block
$block['controls'] = $controls;

$code                  = [];
$code['show_preview']  = 'never';
$code['single_output'] = true;

$code['frontend_html'] = file_get_contents(__DIR__ . '/' . $blockslug . '-template.php');
$code['use_php']       = false;

// Add the code to the block.
$block['code'] = $code;

if (function_exists('lazyblocks')) {
    lazyblocks()->add_block($block);
}
