<?php

$blockslug = 'image';

// Start the block
$block             = [];
$block['title']    = 'Image';
$block['icon']     = 'dashicons dashicons-format-image';
$block['slug']     = 'lazyblock/' . $blockslug;
$block['category'] = 'common';

// Supports
$supports                    = [];
$supports['align']           = [];
$supports['customClassName'] = false;


// Add the support in.
$block['supports'] = $supports;

// Holder for the controls
$controls = [];
///////////// START CONTROLS /////////////

// Image
$control                         = [];
$control['label']                = 'Image';
$control['name']                 = 'image';
$control['type']                 = 'image';
$control['child_of']             = '';

// Make an id.
$control_id = 'control-image-image';

// Add the control to the controls
$controls[$control_id] = $control;


// Radio
$control              = [];
$control['label']     = 'Full Width';
$control['name']      = 'fullwidth';
$control['type']      = 'radio';
$control['placement'] = 'inspector';
$control['required']  = true;
$control['default']   = '0';
$control['child_of']  = '';
$control['choices']   = [
    [
        'label' => 'Yes',
        'value' => '1',
    ],
    [
        'label' => 'No',
        'value' => '0',
    ],
];


// Make an id.
$control_id = 'control-image-fullwidth';

// Add the control to the controls
$controls[$control_id] = $control;

// Alignment
$control              = [];
$control['label']     = 'Alignment';
$control['name']      = 'alignment';
$control['type']      = 'select';
$control['placement'] = 'inspector';
$control['required']  = true;
$control['default']   = 'left';
$control['child_of']  = '';
$control['choices']   = [
    [
        'label' => 'Left',
        'value' => 'justify-content-start',
    ],
    [
        'label' => 'Center',
        'value' => 'justify-content-center',
    ],
    [
        'label' => 'Right',
        'value' => 'justify-content-end',
    ],
];

// Make an id.
$control_id = 'control-image-alignment';

// Add the control to the controls
$controls[$control_id] = $control;

include __DIR__ . '/../image-customs-controls.php';


///////////// END CONTROLS /////////////
// Add the controls to the block
$block['controls'] = $controls;

$code                  = [];
$code['show_preview']  = 'never';
$code['single_output'] = true;

$code['frontend_html'] = file_get_contents(__DIR__ . '/' . $blockslug . '-template.php');
$code['use_php']       = false;

// Add the code to the block.
$block['code'] = $code;

if (function_exists('lazyblocks')) {
    lazyblocks()->add_block($block);
}
