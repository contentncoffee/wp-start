<?php

$blockslug = 'cta';

// Start the block
$block             = [];
$block['title']    = 'CTA';
$block['icon']     = 'dashicons dashicons-controls-play';
$block['slug']     = 'lazyblock/' . $blockslug;
$block['category'] = 'common';

// Supports
$supports                    = [];
$supports['align']           = [];
$supports['customClassName'] = false;


// Add the support in.
$block['supports'] = $supports;

// Holder for the controls
$controls = [];

// Label
$control             = [];
$control['label']    = 'Label';
$control['name']     = 'label';
$control['type']     = 'text';
$control['child_of'] = '';


// Make an id.
$control_id = 'control-cta-label';

// Add the control to the controls
$controls[$control_id] = $control;


// URL
$control             = [];
$control['label']    = 'Url';
$control['name']     = 'url';
$control['type']     = 'url';
$control['child_of'] = '';

// Make an id.
$control_id = 'control-cta-url';

// Add the control to the controls
$controls[$control_id] = $control;


// Style
$control                         = [];
$control['type']                 = 'select';
$control['name']                 = 'buttonstyle';
$control['default']              = 'btn-primary';
$control['label']                = 'Button style';
$control['save_in_meta']         = false;
$control['required']             = false;
$control['child_of']             = '';
$control['hide_if_not_selected'] = 'false';
$control['placement']            = 'inspector';

$control['choices'] = [
    [
        'label' => 'Primary',
        'value' => 'btn-primary',
    ],
    [
        'label' => 'Secondary',
        'value' => 'btn-secondary',
    ],
    [
        'label' => 'Grey',
        'value' => 'btn-grey',
    ],
    [
        'label' => 'Blue',
        'value' => 'btn-blue',
    ],
    [
        'label' => 'Default',
        'value' => 'btn-default',
    ],
];

// Make an id.
$control_id            = 'control-cta-buttonstyle';

// Add the control to the controls
$controls[$control_id] = $control;


// Target
$control              = [];
$control['label']     = 'New Tab?';
$control['name']      = 'target';
$control['type']      = 'radio';
$control['placement'] = 'inspector';
$control['required']  = true;
$control['default']   = '_self';
$control['child_of']  = '';
$control['choices']   = [
    [
        'label' => 'Yes',
        'value' => '_blank',
    ],
    [
        'label' => 'No',
        'value' => '_self',
    ],
];

// Make an id.
$control_id = 'control-cta-target';

// Add the control to the controls
$controls[$control_id] = $control;


// Alignment
$control              = [];
$control['label']     = 'Alignment';
$control['name']      = 'alignment';
$control['type']      = 'radio';
$control['placement'] = 'inspector';
$control['required']  = true;
$control['default']   = 'left';
$control['child_of']  = '';
$control['choices']   = [
    [
        'label' => 'Left',
        'value' => 'left',
    ],
    [
        'label' => 'Center',
        'value' => 'center',
    ],
    [
        'label' => 'Right',
        'value' => 'right',
    ],
];

// Make an id.
$control_id = 'control-cta-alignment';

// Add the control to the controls
$controls[$control_id] = $control;


// Add the controls to the block
$block['controls'] = $controls;

$code                  = [];
$code['show_preview']  = 'never';
$code['single_output'] = true;

$code['frontend_html'] = file_get_contents(__DIR__ . '/' . $blockslug . '-template.php');
$code['use_php']       = false;

// Add the code to the block.
$block['code'] = $code;

if (function_exists('lazyblocks')) {
    lazyblocks()->add_block($block);
}
