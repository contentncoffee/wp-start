<?php

$blockslug = 'news';

// Start the block
$block             = [];
$block['title']    = 'News';
$block['icon']     = 'dashicons dashicons-calendar';
$block['slug']     = 'lazyblock/' . $blockslug;
$block['category'] = 'common';

// Supports
$supports                    = [];
$supports['align']           = [];
$supports['customClassName'] = false;


// Add the support in.
$block['supports'] = $supports;

// Holder for the controls
$controls = [];

// Label
$control             = [];
$control['label']    = 'Amount of news items (newest first)';
$control['name']     = 'amount_of_news_items';
$control['type']     = 'text';
$control['child_of'] = '';


// Make an id.
$control_id = 'control-amount-of-news-items';

// Add the control to the controls
$controls[$control_id] = $control;

// Add the controls to the block
$block['controls'] = $controls;
$code                  = [];
$code['show_preview']  = 'never';
$code['single_output'] = true;

$code['frontend_html'] = file_get_contents(__DIR__.'/'. $blockslug . '-template.php');
$code['use_php']       = true;

// Add the code to the block.
$block['code'] = $code;

if (function_exists('lazyblocks')) {
    lazyblocks()->add_block($block);
}
