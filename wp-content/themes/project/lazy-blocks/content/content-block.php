<?php

$blockslug = 'content';

// Start the block
$block             = [];
$block['title']    = 'Content';
$block['icon']     = 'dashicons dashicons-media-document';
$block['slug']     = 'lazyblock/' . $blockslug;
$block['category'] = 'common';

// Supports
$supports                    = [];
$supports['align']           = [];
$supports['customClassName'] = false;


// Add the support in.
$block['supports'] = $supports;

// Holder for the controls
$controls = [];

// Text
$control                         = [];
$control['label']                = '';
$control['name']                 = 'content';
$control['type']                 = 'classic_editor';
$control['child_of']             = '';

// Make an id.
$control_id = 'control-content-content';

// Add the control to the controls
$controls[$control_id] = $control;


// Add the controls to the block
$block['controls'] = $controls;

$code                  = [];
$code['show_preview']  = 'never';
$code['single_output'] = true;

$code['frontend_html'] = file_get_contents(__DIR__ . '/' . $blockslug . '-template.php');
$code['use_php']       = false;

// Add the code to the block.
$block['code'] = $code;

if (function_exists('lazyblocks')) {
    lazyblocks()->add_block($block);
}
