<div class="container-md">
    <div class="row no-gutters row-cols-1 row-cols-md-2 row-cols-lg-3 row-cols-lg-4 card-deck lazyblock-cards justify-content-lg-between">
        {{#each cards}}
        <div class="col pb-4 d-flex">
            <div class="card px-0">
                <a href="{{url}}">
                <div class="card-img-wrapper">
                    <img data-src="{{image.url}}" loading="lazy" class="card-img-top lazyload"
                         alt="{{title}}">
                </div>
                <div class="card-title">
                    <h3 class="card-title text-center">{{title}}</h3>
                </div>
                </a>
            </div>
        </div>
        {{/each}}
    </div>

</div>