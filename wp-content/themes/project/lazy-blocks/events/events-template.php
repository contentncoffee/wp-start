<div class="container-md">
    <div class="row lazyblock-events">
        <?php
        $args = [
            'post_type' => 'event',
            'posts_per_page' => (int)$attributes['amount_of_events'],
            'order' => 'DESC',
            'orderby' => 'publish_date',
            'meta_key' => 'starting_date',
            'meta_query' => [
                'key' => 'starting_date',
                'value' => date('Ymd'),
                'type' => 'DATE',
                'compare' => '>='
            ]
        ];

        // Maybe it's multilingual
        if (function_exists('pll_current_language')) {
            $args['lang'] = pll_current_language() ? pll_current_language() : pll_default_language();
        }

        // Custom query.
        $myquery = new WP_Query($args);
        // Check that we have query results.
        if ($myquery->have_posts()) {
            // Start looping over the query results.
            while ($myquery->have_posts()) {
                $myquery->the_post();

                ?>
                <div class="col-12 p-4 event-wrapper">
                    <div class="row">
                        <div class="col-lg-4 col-12">
                            <a href="<?= the_permalink() ?>">
                            <?= the_post_thumbnail() ?>
                            </a>
                        </div>
                        <div class="col-lg-8 col-12">
                            <div class="news-item-title"><h3>
                                    <a href="<?= the_permalink() ?>">
                                        <?= get_field('event_title') ?>
                                    </a>
                                </h3></div>
                            <div class="news-item-date"><?= get_field('starting_date') ?></div>
                            <div class="news-item-summary mt-4">
                                <?= !empty(get_field('summary')) ? get_field('summary') : create_fallback_string(get_the_content()); ?>
                            </div>
                            <div class="news-item-visit"><a href="<?= the_permalink() ?>">Read more</a></div>
                        </div>
                    </div>
                </div>

                <?php
            }

            the_posts_pagination(array(
                                     'screen_reader_text' => __('Pages'),
                                     'mid_size' => 1,
                                     'prev_text' => __('Previous'),
                                     'next_text' => __('Next'),
                                 ));


        }
        // Restore original post data.
        wp_reset_postdata();
        ?>

    </div>
    <div class="row lazyblock-events past-events">
        <div class="col-12 pl-4">
            <h3>Past events</h3>
        </div>
        <?php
        $args = [
            'post_type' => 'event',
            'posts_per_page' => (int)$attributes['amount_of_events'],
            'order' => 'DESC',
            'orderby' => 'publish_date',
            'meta_key' => 'starting_date',
            'meta_query' => [
                'key' => 'starting_date',
                'value' => date('Ymd'),
                'type' => 'DATE',
                'compare' => '<'
            ]
        ];

        // Maybe it's multilingual
        if (function_exists('pll_current_language')) {
            $args['lang'] = pll_current_language() ? pll_current_language() : pll_default_language();
        }

        // Custom query.
        $myquery = new WP_Query($args);
        // Check that we have query results.
        if ($myquery->have_posts()) {
            // Start looping over the query results.
            while ($myquery->have_posts()) {
                $myquery->the_post();
                ?>
                <div class="col-12 pl-4 event-wrapper">
                    <a href="<?= the_permalink() ?>">
                        <div class="row">
                            <div class="col-12">
                                <div class="news-item-title"><b><?= the_title(); ?></b>
                                    (<?= get_field('starting_date') ?>)
                                </div>
                            </div>
                        </div>
                    </a>
                </div>

                <?php
            }


            the_posts_pagination(array(
                                     'screen_reader_text' => __('Pages'),
                                     'mid_size' => 1,
                                     'prev_text' => __('Previous'),
                                     'next_text' => __('Next'),
                                 ));
        }
        // Restore original post data.
        wp_reset_postdata();
        ?>
    </div>

</div>