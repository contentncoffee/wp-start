<div class="row lazyblock-twitter-wall">
    <div class="col-12 social-title text-center pt-lg-4">
        <i class="fab fa-twitter mr-2"></i> <h2>{{follow_us_text}}</h2>
    </div>
    <div class="col-12 px-0" style="max-width: 100%;">
        <script src="https://walls.io/js/wallsio-widget-1.2.js" data-wallurl="{{wallurl}}" data-width="100%" data-height="500" data-lazyload="1"></script>
    </div>
</div>