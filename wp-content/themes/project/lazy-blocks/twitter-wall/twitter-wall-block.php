<?php

$blockslug = 'twitter-wall';

// Start the block
$block             = [];
$block['title']    = 'Twitter Wall';
$block['icon']     = 'dashicons dashicons-twitter';
$block['slug']     = 'lazyblock/' . $blockslug;
$block['category'] = 'common';

// Supports
$supports                    = [];
$supports['align']           = [];
$supports['customClassName'] = false;


// Add the support in.
$block['supports'] = $supports;

// Holder for the controls
$controls = [];

// Label
$control             = [];
$control['label']    = 'Follow Us Text';
$control['name']     = 'follow_us_text';
$control['type']     = 'text';
$control['value']    = 'Follow us on Twitter <br />@invest_industry';
$control['child_of'] = '';


// Make an id.
$control_id = 'control-wall-follow-text';

// Add the control to the controls
$controls[$control_id] = $control;

// Label
$control             = [];
$control['label']    = 'Wall Url';
$control['name']     = 'wallurl';
$control['type']     = 'text';
$control['child_of'] = '';


// Make an id.
$control_id = 'control-wall-url';

// Add the control to the controls
$controls[$control_id] = $control;

// Add the controls to the block
$block['controls'] = $controls;

$code                  = [];
$code['show_preview']  = 'never';
$code['single_output'] = true;

$code['frontend_html'] = file_get_contents(__DIR__ . '/' . $blockslug . '-template.php');
$code['use_php']       = false;

// Add the code to the block.
$block['code'] = $code;

if (function_exists('lazyblocks')) {
    lazyblocks()->add_block($block);
}
