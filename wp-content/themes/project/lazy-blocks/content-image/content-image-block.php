<?php

$blockslug = 'content-image';

// Start the block
$block             = [];
$block['title']    = 'Content Image';
$block['icon']     = 'dashicons dashicons-media-document';
$block['slug']     = 'lazyblock/' . $blockslug;
$block['category'] = 'common';

// Supports
$supports                    = [];
$supports['align']           = [];
$supports['customClassName'] = false;


// Add the support in.
$block['supports'] = $supports;

// Holder for the controls
$controls = [];
///////////// START CONTROLS /////////////


// Content
$control                         = [];
$control['label']                = 'Content';
$control['name']                 = 'content';
$control['type']                 = 'inner_blocks';
$control['width']                = '50';
$control['child_of']             = '';

// Make an id.
$control_id = 'control-image-content-content';

// Add the control to the controls
$controls[$control_id] = $control;


// Image
$control                         = [];
$control['label']                = 'Image';
$control['name']                 = 'image';
$control['type']                 = 'image';
$control['width']                = '50';
$control['child_of']             = '';

// Make an id.
$control_id = 'control-image-content-image';

// Add the control to the controls
$controls[$control_id] = $control;

// Radio
$control              = [];
$control['label']     = 'Division';
$control['name']      = 'division';
$control['type']      = 'radio';
$control['placement'] = 'inspector';
$control['required']  = true;
$control['default']   = '6';
$control['child_of']  = '';
$control['choices']   = [
    [
        'label' => '6-6',
        'value' => '6',
    ],
    [
        'label' => '7-5',
        'value' => '7',
    ],
    [
        'label' => '8-4',
        'value' => '8',
    ],
    [
        'label' => '9-3',
        'value' => '9',
    ],
];


// Make an id.
$control_id = 'control-content-image-division';

// Add the control to the controls
$controls[$control_id] = $control;


include __DIR__ . '/../image-customs-controls.php';


// Add the controls to the block
$block['controls'] = $controls;

///////////// END CONTROLS /////////////
// Add the controls to the block
$block['controls'] = $controls;

$code                  = [];
$code['show_preview']  = 'never';
$code['single_output'] = true;

$code['frontend_html'] = file_get_contents(__DIR__ . '/' . $blockslug . '-template.php');
$code['use_php']       = false;

// Add the code to the block.
$block['code'] = $code;

if (function_exists('lazyblocks')) {
    lazyblocks()->add_block($block);
}
