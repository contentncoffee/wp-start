<span>
{{#include 'image-customs-style'}}
<div class="row lazyblock-content-image">
    <div class="col-lg-{{division}}">
        {{{content}}}
    </div>
    <div class="col-lg-{{#leftover division}}">
        <img class="on-the-fly-{{#blockid}}" src="{{image.url}}" alt="{{image.alt}}" title="{{image.alt}}" />
    </div>
</div>
</span>