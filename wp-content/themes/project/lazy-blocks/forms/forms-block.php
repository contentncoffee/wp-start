<?php

$blockslug = 'forms';

// Start the block
$block             = [];
$block['title']    = 'Form';
$block['icon']     = 'dashicons dashicons-controls-play';
$block['slug']     = 'lazyblock/' . $blockslug;
$block['category'] = 'common';

// Supports
$supports                    = [];
$supports['align']           = [];
$supports['customClassName'] = false;


// Add the support in.
$block['supports'] = $supports;

// Holder for the controls
$controls = [];

// Label
$control             = [];
$control['label']    = 'Label';
$control['name']     = 'label';
$control['type']     = 'text';
$control['child_of'] = '';


// Make an id.
$control_id = 'control-cta-label';

// Add the control to the controls
$controls[$control_id] = $control;

// Form selections
$control                         = [];
$control['type']                 = 'select';
$control['name']                 = 'form';
$control['default']              = 'contact';
$control['label']                = 'Form';
$control['save_in_meta']         = false;
$control['required']             = false;
$control['child_of']             = '';
$control['hide_if_not_selected'] = 'false';
$control['placement']            = 'inspector';

$control['choices'] = [
    [
        'label' => 'Contact',
        'value' => 'contact',
    ],
    [
        'label' => 'Newsletter',
        'value' => 'subscribe',
    ],
];

// Make an id.
$control_id            = 'control-form';

// Add the control to the controls
$controls[$control_id] = $control;

// Add the controls to the block
$block['controls'] = $controls;

$code                  = [];
$code['show_preview']  = 'never';
$code['single_output'] = true;

$code['frontend_html'] = file_get_contents(__DIR__ . '/' . $blockslug . '-template.php');
$code['use_php']       = true;

// Add the code to the block.
$block['code'] = $code;

if (function_exists('lazyblocks')) {
    lazyblocks()->add_block($block);
}
