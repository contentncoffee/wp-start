<?php

$blockslug = 'section';

// Start the block
$block             = [];
$block['title']    = 'Section';
$block['icon']     = 'dashicons dashicons-tablet';
$block['slug']     = 'lazyblock/' . $blockslug;
$block['category'] = 'common';

// Supports
$supports                    = [];
$supports['align']           = [];
$supports['customClassName'] = false;


// Add the support in.
$block['supports'] = $supports;

// Holder for the controls
$controls = [];

// Background
$control              = [];
$control['label']     = 'Background';
$control['name']      = 'background';
$control['type']      = 'radio';
$control['placement'] = 'inspector';
$control['required']  = true;
$control['default']   = 'white';
$control['child_of']  = '';

$control['choices'] = [
    [
        'label' => 'White',
        'value' => 'white',
    ],
    [
        'label' => 'Grey',
        'value' => 'grey',
    ],
];

// Make an id.
$control_id = 'control-section-background';

// Add the control to the controls
$controls[$control_id] = $control;

// Squish
$control              = [];
$control['label']     = 'Squeeze';
$control['name']      = 'squeeze';
$control['type']      = 'radio';
$control['placement'] = 'inspector';
$control['required']  = true;
$control['default']   = '0';
$control['child_of']  = '';

$control['choices'] = [
    [
        'label' => 'None',
        'value' => '0',
    ],
    [
        'label' => '1-10-1',
        'value' => '10',
    ],
    [
        'label' => '2-8-2',
        'value' => '8',
    ],
    [
        'label' => '3-6-3',
        'value' => '6',
    ],
    [
        'label' => '4-4-4',
        'value' => '4',
    ],
];


// Make an id.
$control_id = 'control-section-squeeze';

// Add the control to the controls
$controls[$control_id] = $control;

// Content
$control             = [];
$control['label']    = '';
$control['name']     = 'content';
$control['type']     = 'inner_blocks';
$control['child_of'] = '';


// Make an id.
$control_id = 'control-section-content';

// Add the control to the controls
$controls[$control_id] = $control;

// Add the controls to the block
$block['controls'] = $controls;

$code                  = [];
$code['show_preview']  = 'never';
$code['single_output'] = true;

$code['frontend_html'] = file_get_contents(__DIR__ . '/' . $blockslug . '-template.php');
$code['use_php']       = false;

// Add the code to the block.
$block['code'] = $code;

if (function_exists('lazyblocks')) {
    lazyblocks()->add_block($block);
}
