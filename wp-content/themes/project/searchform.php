<?php
/**
 * The searchform.php template.
 *
 * Used any time that get_search_form() is called.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package WordPress
 * @subpackage Twenty_Twenty
 * @since Twenty Twenty 1.0
 */

/*
 * Generate a unique ID for each form and a string containing an aria-label
 * if one was passed to get_search_form() in the args array.
 */
$aria_label = !empty($args['label']) ? 'aria-label="' . esc_attr($args['label']) . '"' : '';
?>
<form role="search" <?php echo $aria_label; // phpcs:ignore WordPress.Security.EscapeOutput.OutputNotEscaped -- Escaped above. ?>
      method="get" class="search-form" action="<?php echo esc_url(home_url('/')); ?>">

    <input type="search" id="search_field" class="search-field"
           placeholder="<?php echo esc_attr_x('Search &hellip;', 'placeholder'); ?>"
           value="<?php echo get_search_query(); ?>" name="s"/>

    <button type="submit" class="search-submit"><i class="dashicons dashicons-search"></i></button>
</form>
