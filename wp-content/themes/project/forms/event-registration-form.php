<?php if ($event_details['allow_registrations']) { ?>
    <?php if (strtotime($event_details_raw['registrations_starting_date']) <= time()) { ?>
        <?php if (strtotime($event_details_raw['registrations_ending_date']) >= time()) { ?>
            <h2>Registration <span>form</span></h2>
            <form id="event-registration-form" method="post" action="">
                <div class="row">
                    <?php if ($event_details['first_name_field']) { ?>
                        <div class="col-12 col-lg-6">
                            <div class="form-group">
                                <label for="first_name"><?= pll__('first_name') ?> <?= $event_details['first_name_field'] == 2 ? '*' : '' ?></label>
                                <input type="text" class="form-control" name="first_name"
                                       id="first_name" <?= $event_details['first_name_field'] == 2 ? 'required' : '' ?> >
                            </div>
                        </div>
                    <?php } ?>
                    <?php if ($event_details['last_name_field']) { ?>
                        <div class="col-12 col-lg-6">
                            <div class="form-group">
                                <label for="last_name"><?= pll__('last_name') ?> <?= $event_details['last_name_field'] == 2 ? '*' : '' ?></label>
                                <input type="text" class="form-control" name="last_name"
                                       id="last_name" <?= $event_details['last_name_field'] == 2 ? 'required' : '' ?> >
                            </div>
                        </div>
                    <?php } ?>
                    <?php if ($event_details['organisation_field']) { ?>
                        <div class="col-12 col-lg-6">
                            <div class="form-group">
                                <label for="organisation"><?= pll__('organisation') ?> <?= $event_details['organisation_field'] == 2 ? '*' : '' ?></label>
                                <input type="text" class="form-control" name="organisation"
                                       id="organisation" <?= $event_details['organisation_field'] == 2 ? 'required' : '' ?> >
                            </div>
                        </div>
                    <?php } ?>
                    <?php if ($event_details['job_title_field']) { ?>
                        <div class="col-12 col-lg-6">
                            <div class="form-group">
                                <label for="job_title"><?= pll__('job_title') ?> <?= $event_details['job_title_field'] == 2 ? '*' : '' ?></label>
                                <input type="text" class="form-control" name="job_title"
                                       id="job_title" <?= $event_details['job_title_field'] == 2 ? 'required' : '' ?> >
                            </div>
                        </div>
                    <?php } ?>
                    <?php if ($event_details['telephone_number_field']) { ?>
                        <div class="col-12 col-lg-6">
                            <div class="form-group">
                                <label for="telephone_number"><?= pll__('telephone_number') ?> <?= $event_details['telephone_number_field'] == 2 ? '*' : '' ?></label>
                                <input type="text" class="form-control" name="telephone_number"
                                       id="telephone_number" <?= $event_details['telephone_number_field'] == 2 ? 'required' : '' ?> >
                            </div>
                        </div>
                    <?php } ?>
                    <div class="col-12 col-lg-6">
                        <div class="form-group">
                            <label for="email"><?= pll__('email') ?> *</label>
                            <input type="email" class="form-control" name="email" id="email" required>
                        </div>
                    </div>
                    <?php if ($event_details['dietary_restrictions_field']) { ?>
                        <div class="col-12 col-lg-6">
                            <div class="form-group">
                                <label for="dietary_restrictions"><?= pll__('dietary_restrictions') ?> <?= $event_details['dietary_restrictions_field'] == 2 ? '*' : '' ?></label>
                                <input type="text" class="form-control" name="dietary_restrictions"
                                       id="dietary_restrictions" <?= $event_details['dietary_restrictions_field'] == 2 ? 'required' : '' ?>>
                            </div>
                        </div>
                    <?php } ?>
                    <?php if ($event_details['ask_for_european_parliament_badge']) { ?>
                        <div class="col-12 mb-2">
                            <?= pll__('has_badge_for_european_parliament') ?>
                            <div class="form-check">
                                <input type="radio" name="has_badge_for_european_parliament" value="1" checked
                                       class="form-check-input"
                                       id="european_parliament_badge_yes" data-toggle='collapse'
                                       data-target='#european_parliament_badge_collapse.show'>
                                <label class="form-check-label"
                                       for="european_parliament_badge_yes"><?= pll__('yes') ?></label>
                            </div>
                            <div class="form-check">
                                <input type="radio" name="has_badge_for_european_parliament" value="0"
                                       class="form-check-input"
                                       id="european_parliament_badge_no" data-toggle='collapse'
                                       data-target='#european_parliament_badge_collapse:not(.show)'>
                                <label class="form-check-label"
                                       for="european_parliament_badge_no"><?= pll__('no') ?></label>
                            </div>

                            <div id='european_parliament_badge_collapse' class='collapse mt-2'>
                                <div class="row">

                                    <div class="col-12 col-lg-6">
                                        <div class="form-group">
                                            <label for="date_of_birth"><?= pll__('date_of_birth') ?> *</label>
                                            <input type="text" class="form-control" name="date_of_birth"
                                                   id="date_of_birth"
                                                   data-inputmask-alias="datetime"
                                                   data-inputmask-inputformat="dd/mm/yyyy"
                                                   data-inputmask-placeholder="dd/mm/yyyy">
                                        </div>
                                    </div>

                                    <div class="col-12 col-lg-6">
                                        <div class="form-group">
                                            <label for="nationality"><?= pll__('nationality') ?> *</label>
                                            <input type="text" class="form-control" name="nationality" id="nationality">
                                        </div>
                                    </div>

                                    <div class="col-12 col-lg-6">
                                        <div class="form-group">
                                            <label for="identification_available"><?= pll__('identification_available') ?> *</label>
                                            <select class="form-control" name="identification_available"
                                                   id="identification_available">
                                                <option value="1"><?= pll__('identification_id_card_number'); ?></option>
                                                <option value="2"><?= pll__('identification_passport_number'); ?></option>
                                            </select>
                                        </div>
                                    </div>

                                    <div class="col-12 col-lg-6">
                                        <div class="form-group">
                                            <label for="id_card_number"><?= pll__('id_card_number') ?> *</label>
                                            <input type="text" class="form-control" name="id_card_number"
                                                   id="id_card_number">
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>
                    <?php } ?>

                    <?php if ($event_details['question_1_field']) { ?>
                        <div class="col-12 col-lg-6">
                            <div class="form-group">
                                <label for="question_1"><?= $event_details['question_1'] ?> <?= $event_details['question_1_field'] == 2 ? '*' : '' ?></label>
                                <input type="text" class="form-control" name="question_1"
                                       id="question_1" <?= $event_details['question_1_field'] == 2 ? 'required' : '' ?> >
                            </div>
                        </div>
                    <?php } ?>

                    <?php if ($event_details['question_2_field']) { ?>
                        <div class="col-12 col-lg-6">
                            <div class="form-group">
                                <label for="question_2"><?= $event_details['question_2'] ?> <?= $event_details['question_2_field'] == 2 ? '*' : '' ?></label>
                                <input type="text" class="form-control" name="question_2"
                                       id="question_2" <?= $event_details['question_2_field'] == 2 ? 'required' : '' ?> >
                            </div>
                        </div>
                    <?php } ?>

                    <?php if ($event_details['question_3_field']) { ?>
                        <div class="col-12 col-lg-6">
                            <div class="form-group">
                                <label for="question_3"><?= $event_details['question_3'] ?> <?= $event_details['question_3_field'] == 2 ? '*' : '' ?></label>
                                <input type="text" class="form-control" name="question_3"
                                       id="question_3" <?= $event_details['question_3_field'] == 2 ? 'required' : '' ?> >
                            </div>
                        </div>
                    <?php } ?>

                    <?php if ($event_details['gdpr_consent_field_1']): ?>
                        <div class="col-12">
                            <div class="form-group">
                                <div class="form-check">
                                    <input class="form-check-input" name="gdpr_consent_1" value="1" type="checkbox"
                                           id="gdpr_consent_1" required>
                                    <label class="form-check-label" for="gdpr_consent_1">
                                        <?= pll__('gdpr_consent_1_label') ?>
                                    </label>
                                </div>
                            </div>
                        </div>
                    <?php endif; ?>

                    <?php if ($event_details['gdpr_consent_field_2']): ?>
                        <div class="col-12">
                            <div class="form-group">
                                <div class="form-check">
                                    <input class="form-check-input" name="gdpr_consent_2" value="1" type="checkbox"
                                           id="gdpr_consent_2" required>
                                    <label class="form-check-label" for="gdpr_consent_2">
                                        <?= pll__('gdpr_consent_2_label') ?>
                                    </label>
                                </div>
                            </div>
                        </div>
                    <?php endif; ?>
                    <?php if ($event_details['gdpr_consent_field_3']): ?>
                        <div class="col-12">
                            <div class="form-group">
                                <div class="form-check">
                                    <input name="gdpr_consent_3" type="hidden" value="0">
                                    <input class="form-check-input" name="gdpr_consent_3" value="1" type="checkbox"
                                           id="gdpr_consent_3">
                                    <label class="form-check-label" for="gdpr_consent_3">
                                        <?= pll__('gdpr_consent_3_label') ?>
                                    </label>
                                </div>
                            </div>
                        </div>
                    <?php endif; ?>


                    <?php if ($event_details['newsletter_field']): ?>
                    <div class="col-12">
                        <div class="form-group">
                            <div class="form-check">
                                <input name="newsletter" type="hidden" value="0">
                                <input class="form-check-input" name="newsletter" value="1" type="checkbox"
                                       id="newsletter">
                                <label class="form-check-label" for="newsletter">
                                    <?= pll__('newsletter_label') ?>
                                </label>
                            </div>
                        </div>
                    </div>
                    <?php endif; ?>
                    <div class="col-12">
                        <div class="form-group">
                            <div class="form-check">
                                <label class="form-check-label">
                                    * <?= pll__('required_fields') ?>
                                </label>
                            </div>
                        </div>
                    </div>
                    <div class="col-12">
                        <input type="hidden" name="event" value="<?= $event->ID ?>">
                        <button type="submit" class="btn btn-primary"><?= pll__('submit_registration') ?></button>

                    </div>
                </div>
            </form>
            <div id="event-registration-form-thankyou" class="d-none">
                <h3><?= pll__('event-registration-form-thankyou-title') ?></h3>
                <p>
                    <?= pll__('event-registration-form-thankyou-text') ?>
                </p>
            </div>
            <script>
                jQuery(document).ready(function ($) {
                    // Initialize InputMask
                    $(":input").inputmask();
                });

                function grayer(formId, yesNo) {
                    var f = document.getElementById(formId), s, opacity;
                    s = f.style;
                    opacity = yesNo ? '40' : '100';
                    s.opacity = s.MozOpacity = s.KhtmlOpacity = opacity / 100;
                    s.filter = 'alpha(opacity=' + opacity + ')';
                    for (var i = 0; i < f.length; i++) f[i].disabled = yesNo;
                }

                //Toggle restrictions based on having badge
                jQuery('#european_parliament_badge_no, #european_parliament_badge_yes').on('change', function () {
                    if (jQuery('#european_parliament_badge_no').is(':checked')) {
                        jQuery('#european_parliament_badge_collapse :input').each(function () {
                            jQuery(this).attr('required', true).prop('required', true);
                        });
                    } else {
                        jQuery('#european_parliament_badge_collapse :input').each(function () {
                            jQuery(this).removeAttr('required').prop('required', false);
                        });
                    }
                });

                jQuery('form').on('blur', ':input[required]', function () {
                    if (jQuery(this).val() === '') {
                        jQuery(this).css({borderColor: 'red'});
                    } else {
                        jQuery(this).css({borderColor: '#ced4da'});
                    }
                });

                // Submit a form using JS not the traditional way ;)
                // Insteqd of click you will use submit
                jQuery('#event-registration-form').on('submit', function (e) {

                    // Don't actually submit the form.
                    e.preventDefault();

                    // Gather the data in the form.
                    // For real you probably will user jQuery('#testform').serialize()
                    var data = jQuery('#event-registration-form').serialize()

                    // This would be a great place to hide your form and show a spinner
                    grayer('event-registration-form', true);

                    // Execute the google recaptcha browser part
                    grecaptcha.execute('<?= ccfigValue('recaptcha-public-token')?>', {action: 'event_registration_<?= $event->ID ?>'}).then(function (token) {

                        // Add the token response to the data of the form.
                        data = data + '&token=' + token;

                        // Post the data of the form to ccforms plugin.
                        jQuery.post('/wp-json/ccevents/v1/form', data, function (response) {

                            // This should be 1 of 2 things: OK or NOK
                            //console.log(response);

                            // This would be a great place to stop showing your spinner and instead show a thank you.
                            // Or error message...

                            if (response == 'OK') {
                                jQuery('#event-registration-form').addClass('d-none');
                                jQuery('#event-registration-form-thankyou').removeClass('d-none');
                            } else {
                                grayer('event-registration-form', false);
                            }

                        });
                    });
                });

            </script>

        <?php } ?>
    <?php } ?>
<?php } ?>