<div class="contact-form">
    <form action="" id="contact-form">
        <input type="hidden" name="form_id" value="contact">
        <input type="hidden" name="locale" value="<?= get_locale() ?>">
        <div class="row">
            <div class="col-12 col-lg-6">
                <div class="form-group">
                    <input type="text" class="form-control" required name="firstname" placeholder="<?php echo pll__('first_name'); ?> *">
                </div>
            </div>
            <div class="col-12 col-lg-6">
                <div class="form-group">
                    <input type="text" class="form-control" required name="lastname" placeholder="<?php echo pll__('last_name'); ?> *">
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-12 col-lg-6">
                <div class="form-group">
                    <input type="email" class="form-control" required name="email" placeholder="<?php echo pll__('email'); ?> *">
                </div>
            </div>
            <div class="col-12 col-lg-6">
                <input type="text" class="form-control" required name="subject" placeholder="<?php echo pll__('subject'); ?> *">
            </div>
        </div>
        <div class="row">
            <div class="col-12">
                <div class="form-group">
                    <textarea name="message" placeholder="Message *" class="form-control" rows="10"></textarea>
                </div>
            </div>
            <div class="col-12">
                <div class="form-group">
                    <div class="form-check">
                        <label class="form-check-label">
                            * <?= pll__('required_fields') ?>
                        </label>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-12">
                <div class="form-group text-center">
                    <input type="submit" id="contactSubmit" value="Send" class="btn btn-primary" />
                </div>
            </div>
        </div>
    </form>
    <div id="contact-spinner" class="d-none text-center">
        Loading<br />
        <i class="fas fa-spin fa-circle-notch"></i>
    </div>
    <div id="contact-thankyou" class="d-none">
        <b>Thank you for your message</b><br />
        <br />
        We will review and revert promptly. <br />
        The secretariat
    </div>
</div>
<script>
    // Submit a form using JS not the traditional way ;)
    // Instead of click you will use submit
    jQuery('#contact-form').on('submit', function(e){

        // Don't actually submit the form.
        e.preventDefault();

        // Gather the data in the form.
        // For real you probably will user jQuery('#testform').serialize()
        var data = jQuery('#contact-form').serialize();
        jQuery('#contact-form').addClass('d-none');
        jQuery('#contact-spinner').removeClass('d-none');
        // This would be a great place to hide your form and show a spinner

        // Execute the google recaptcha browser part
        grecaptcha.execute('<?= ccfigValue('recaptcha-public-token') ?>', {action: 'contact'}).then(function (token) {

            // Add the token response to the data of the form.
            data = data + '&token=' + token;
            console.log(data);
            // Post the data of the form to ccforms plugin.
            jQuery.post('/wp-json/ccforms/v1/form', data, function (response) {

                // This should be 1 of 2 things: OK or NOK
                console.log(response);

                // This would be a great place to stop showing your spinner and instead show a thank you.
                // Or error message...
                jQuery('#contact-spinner').addClass('d-none');
                jQuery('#contact-thankyou').removeClass('d-none');
            });
        });
    });
</script>