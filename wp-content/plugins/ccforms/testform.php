<?php
/**
 * This is a test form that you can use to see the mechanics of what to do.
 *
 * Each form must send in a:
 *  form_id
 *  email
 *  locale
 *
 * The google recaptcha public token should replace the token seen here.
 *
 * <?= ccfigValue('recaptcha-secret-token') ?>
 */
?>
<html>
<head>
    <script src="https://www.google.com/recaptcha/api.js?render=6LcxzMYUAAAAAHye8KGmskEwPFPOLTgEnMaeq4Gv"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
</head>
<body>

<!-- let's pretend like we have a form here -->
<input type="button" id="testform" value="click" />

<script>
  // Submit a form using JS not the traditional way ;)
  // Insteqd of click you will use submit
    jQuery('#testform').on('click', function(e){

        // Don't actually submit the form.
        e.preventDefault();

        // Gather the data in the form.
      // For real you probably will user jQuery('#testform').serialize()
        var data = {
            form_id: 'testform',
              email: 'mathias.selleslach@contentcoffee.com',
          other_value: 'Here I am, Rock me like a hurricane!'
          locale: 'en_US'
        };

        // This would be a great place to hide your form and show a spinner

      // Execute the google recaptcha browser part
      grecaptcha.execute('6LcxzMYUAAAAAHye8KGmskEwPFPOLTgEnMaeq4Gv', {action: 'testform'}).then(function (token) {

          // Add the token response to the data of the form.
          data.token = token;

          // Post the data of the form to ccforms plugin.
          jQuery.post('/wp-json/ccforms/v1/form', data, function (response) {

              // This should be 1 of 2 things: OK or NOK
              console.log(response);

              // This would be a great place to stop showing your spinner and instead show a thank you.
              // Or error message...
          });
      });
    });
</script>


</body>
</html>
