<?php
/*
Plugin Name: CC Forms
Description: Creates a post type and provides functionality for creating and managing form entries.
Version:     1.0.0
Author:      CC
*/

// Helper functions.
require_once dirname(__FILE__) . '/helpers.php';

/**
 * Register the form entry type
 */
function ccforms_register_formentry()
{
    register_post_type(
        'formentry',
        [
        'labels' => [
          'name' => 'Form Entries',
          'singular_name' => 'Form Entry',
          'menu_name' => 'Form Entries',
          'all_items' => 'All Entries',
          'edit_item' => 'Edit Entry',
          'search_items' => 'Search Configs',
        ],
        'public' => false,
        'has_archive' => false,
        'show_ui' => true,
        'show_in_menu' => true,
        'supports' => ['title'],
        'capability_type' => 'post',
        'capabilities' => [
          'create_posts' => 'do_not_allow', // false < WP 4.5, credit @Ewout
        ],
        'map_meta_cap' => true,
        ]
    );
}

add_action('init', 'ccforms_register_formentry');


/*
 * Add columns to form entries post list
 */
function ccforms_add_formentry_columns($columns)
{
    return array_merge(
        $columns,
        [
        'form_id' => 'Form ID',
        'email' => 'Email',
        'locale' => 'Language',
        ]
    );
}

add_filter('manage_formentry_posts_columns', 'ccforms_add_formentry_columns');

/*
 * Define the columns for the form entries post list.
 */
function ccforms_formentry_custom_column($column, $post_id)
{
    switch ($column) {
        case 'form_id':
            echo get_post_meta($post_id, 'form_id', true);
            break;
        case 'email':
            echo get_post_meta($post_id, 'email', true);
            break;
        case 'locale':
            echo get_post_meta($post_id, 'locale', true);
            break;
    }
}

add_action('manage_formentry_posts_custom_column', 'ccforms_formentry_custom_column', 10, 2);


/**
 * Adds a button to the admin page for form entries.
 *
 * @param $type
 */
function ccforms_add_export_button($type)
{
    if ($type == 'formentry') {
        $form_ids = _get_all_meta_values('formentry', 'form_id');
        ?>
      <span id="export_form_entries_wrapper">
          <select name="form_id" style="float: none;">
            <option value="">All</option>
            <?php foreach ($form_ids as $form_id) { ?>
              <option value="<?php echo $form_id; ?>"><?php echo nice_name($form_id); ?></option>
            <?php } ?>
          </select>
          <input type="submit" name="export_form_entries" id="export_form_entries" class="button"
                 style="margin: 1px 8px 0 0;" value="Export All">
        </span>

      <script type="text/javascript">
        jQuery(function ($) {
          $('#export_form_entries_wrapper').insertAfter('#post-query-submit');
          $('#post-query-submit').hide();
        });
      </script>
        <?php
    }
}

/**
 * Add a button to export the form entries when viewing them in the admin.
 */
add_action('restrict_manage_posts', 'ccforms_add_export_button', 10, 1);

/**
 * Handle the export of form entries to a csv file.
 */
function ccforms_export_form_entries()
{
    if (is_user_logged_in() && is_admin() && isset($_GET['export_form_entries'])) {
        // Did we have a form id in the url vars?
        $form_id = false;
        if (isset($_GET['form_id']) && !empty($_GET['form_id'])) {
            $form_id = $_GET['form_id'];
        }

        $args = [
          'post_type' => ['formentry'],
          'order' => 'DESC',
          'orderby' => 'title',
          'post_status' => 'publish',
          'posts_per_page' => -1,
        ];

        // If we had a form id then add that to the query.
        if ($form_id) {
            $args['meta_query'] = [
              'relation' => 'AND',
              [
                'key' => 'form_id',
                'value' => $form_id,
                'compare' => '=',
              ],
            ];
        }

        // The Query
        $my_query = new WP_Query($args);

        // Only do this if the person is logged and has role editor or administrator.
        $user = _wp_get_current_user();
        $roles_user = $user->roles;
        $roles_required = ['administrator', 'editor'];
        $roles_found = array_intersect($roles_required, $roles_user);
        if (count($roles_found) > 0 &&
          $my_query->post_count > 0
        ) {
            $filename = "form_entries_export";
            if ($form_id) {
                $filename = $form_id . '-export-' . wp_date('Y-m-d-h-i');
            }
            $filename .= '.csv';

            // Output the headers for a csv file.
            header('Content-type: text/csv');
            header('Content-Disposition: attachment; filename="' . $filename . '"');
            header('Pragma: no-cache');
            header('Expires: 0');

            $file = fopen('php://output', 'w');

            // Start the headers of the CSV.
            $headers = [
              'Form ID',
              'Received',
              'Email',
              'Locale',
            ];
            $headers = apply_filters('ccforms_form_entries_headers', $headers, $form_id);
            // Output the first row/headers.
            fputcsv(
                $file,
                convertToISOCharset($headers)
            );

            // Now the Data.
            while ($post = @$my_query->next_post()) {
                /** @var WP_Post $post */

                // These are the normal fields that come in.
                $fields = [
                  'form_id',
                  'received',
                  'email',
                  'locale',
                ];
                $fields = apply_filters('ccforms_form_entries_fields', $fields, $form_id);

                // Build up a bag of values for output.
                $values = [];
                foreach ($fields as $field) {
                    $values[] = get_post_meta($post->ID, $field, true);
                }

                // Output the data.
                fputcsv(
                    $file,
                    convertToISOCharset($values)
                );
            }

            // Be done with it and move on.
            exit();
        }
    }
}

add_action('init', 'ccforms_export_form_entries');

// Add an endpoints to handle the forms.
add_action('rest_api_init', function () {
    register_rest_route('ccforms/v1', '/form', [
      'methods' => 'POST',
      'callback' => 'ccforms_form',
      'permission_callback' => function () {
          return true;
      },
    ]);
});

function ccforms_form(WP_REST_Request $request)
{
    // You can get the combined, merged set of parameters:
    $parameters = $request->get_params();


    // Allowed form ids to be sent in.
    $form_ids = [];
    $form_ids = apply_filters('ccforms_form_ids', $form_ids);
    if (!$form_ids || !is_array($form_ids)) {
        return "NOK";
    }


    // All forms save to an entry and all forms post to slack.
    // These forms also send an email.
    $sends = [];
    $sends = apply_filters('ccforms_sends', $sends);
    if (!$sends || !is_array($sends)) {
        $sends = [];
    }

    // Every form has to have an ID, Email and Token.
    // The ID has to be in the above array, Email valid, and the token has to be human.
    if (isset($parameters['form_id']) &&
      !empty($parameters['form_id']) &&
      in_array($parameters['form_id'], $form_ids) &&

      isset($parameters['email']) &&
      !empty($parameters['email']) &&
      filter_var($parameters['email'], FILTER_VALIDATE_EMAIL) &&

      isset($parameters['token']) &&
      !empty($parameters['token']) &&
      ccforms_verify_captcha_token($parameters['token'], $parameters['form_id'])
    ) {
        // At this point let's start something but we still need to see.
        $postMeta = [];

        $title = wp_date('Y-m-d H:i:s');
        $parameters['received'] = $title;
        // Thank you rest api for not respecting the language of the user...
        $locales = [];
        $locales[] = 'en_US';
        $locales[] = 'fr_BE';
        $locales[] = 'nl_BE';

        if (!isset($parameters['locale']) || !in_array($parameters['locale'], $locales)) {
            $parameters['locale'] = get_locale();
        }

        // Lets take from the form just what we need.
        $dataDesired = [
          'email',
          'locale',
          'form_id',
          'received',
        ];

        // Additional field besides the basics.
        $dataDesired = apply_filters('ccforms_form_entries_fields', $dataDesired, $parameters['form_id']);

        // Start a blank string for putting in the message later.
        $messageData = '';

        foreach ($dataDesired as $key) {
            if (isset($parameters[$key]) && !empty($parameters[$key])) {
                // Nothing longer than 500 bytes, even then still...
                $postMeta[$key] = substr($parameters[$key], 0, 500);
            } else {
                $postMeta[$key] = '';
            }
            $messageData .= str_pad($key, 20, ' ') . ': ' . $postMeta[$key] . "\n";
        }

        $hostname = $_SERVER['HTTP_HOST'];

        $message = "";
        $message .= "Team,\n\nA user filled in the " . $parameters['form_id'] . " form on the $hostname website.\n\n";
        $message .= "Here is their data:\n\n";
        $message .= $messageData;
        $message .= "\n\nWe hope that you are having a great day!";
        $message .= "\n\n- The Website";

        if (in_array($parameters['form_id'], $sends)) {
            // Set a to address that we will send the form entry to.
            // $to = 'mathias.selleslach@contentcoffee.com';
            $to = '';
            $to = apply_filters('ccforms_to', $to, $parameters['form_id']);

            // If the result is a valid email send it.
            if (filter_var($to, FILTER_VALIDATE_EMAIL)) {
                wp_mail($to, "Form Entry: " . $parameters['form_id'], $message);
            }

            if ($to != 'mathias.selleslach@contentcoffee.com') {
                // Send me a back up email no matter what, you never know.
                // wp_mail('mathias.selleslach@contentcoffee.com', "Form Entry: " . $parameters['form_id'], $message);
            }
        }

        // All forms save to an entry and all forms post to slack.
        // These forms also send an email.
        $doNotStore = apply_filters('ccforms_do_not_store_in_db', []);

        if (!$doNotStore || !is_array($doNotStore)) {
            $doNotStore = [];
        }

        if (in_array($parameters['form_id'], $doNotStore) === false) {
            // Now create a form entry:
            $post = [];
            $post['id'] = 0;
            $post['post_title'] = $title;
            $post['post_type'] = 'formentry';
            $post['post_status'] = 'publish';
            $post['meta_input'] = $postMeta;

            wp_insert_post($post);
        }


        // Send this to our slack channel.
        //        $ch   = curl_init('some-slack-url');
        //        $data = [
        //            'payload' => json_encode(
        //                [
        //                    "text" => $message,
        //                ]
        //            ),
        //        ];
        //
        //        curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
        //        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'POST');
        //        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        //        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        //        $result = curl_exec($ch);
        //        curl_close($ch);

        return "OK";
    }

    return "NOK THIS";
}

/**
 * Verify the google recaptcha.
 *
 * @param $token
 * @param $action
 *
 * @return bool
 */
function ccforms_verify_captcha_token($token, $action)
{
    // build up and check the recpatcha.
    $secretKey = '-----';
    $secretKey = apply_filters('ccforms_recaptcha_secret_token', $secretKey);

    // post request to server
    $url = 'https://www.google.com/recaptcha/api/siteverify';
    $data = ['secret' => $secretKey, 'response' => $token];

    // Curl the google and find out if this is a human.
    $verify = curl_init();
    curl_setopt($verify, CURLOPT_URL, $url);
    curl_setopt($verify, CURLOPT_POST, true);
    curl_setopt($verify, CURLOPT_POSTFIELDS, http_build_query($data));
    curl_setopt($verify, CURLOPT_SSL_VERIFYPEER, false);
    curl_setopt($verify, CURLOPT_RETURNTRANSFER, true);
    $response = curl_exec($verify);
    $response = json_decode($response, true);

    // Is success, is human, is the action we were looking for?
    return $response['success'] && $response['score'] >= 0.5 && $response['action'] == $action;
}


// Example of adding headers and fields to the export and form data collection.
// Examples on how to use the filters with the plugin.

add_filter('ccforms_form_entries_headers', 'ccforms_testform_headers', 10, 2);
add_filter('ccforms_form_entries_fields', 'ccforms_testform_fields', 10, 2);
add_filter('ccforms_form_ids', 'ccforms_form_testform_ids', 10, 1);
add_filter('ccforms_sends', 'ccforms_form_testform_sends', 10, 1);
add_filter('ccforms_do_not_store_in_db', 'ccforms_form_testform_do_not_store_in_db', 10, 1);
add_filter('ccforms_to', 'ccforms_form_testform_to', 10, 2);

function ccforms_testform_headers($headers, $form_id)
{
    if ($form_id == 'testform') {
        $headers[] = 'Other Value';
    }

    return $headers;
}

function ccforms_testform_fields($fields, $form_id)
{
    if ($form_id == 'testform') {
        $fields[] = 'other_value';
    }

    return $fields;
}

function ccforms_form_testform_ids($form_ids)
{
    $form_ids[] = 'testform';

    return $form_ids;
}

function ccforms_form_testform_sends($form_ids)
{
    $form_ids[] = 'testform';

    return $form_ids;
}

function ccforms_form_testform_do_not_store_in_db($form_ids)
{
    $form_ids[] = 'testform';

    return $form_ids;
}

function ccforms_form_testform_to($to, $form_id)
{
    if ($form_id == 'testform') {
        $to = 'mathias.selleslach@contentcoffee.com';
    }

    return $to;
}
