<?php
/*
Plugin Name: Project
Plugin URI: https://www.isabelgroup.eu
Description: Custom Project Code
Author: webmaster
Version: 1.0
*/

add_filter('auto_update_plugin', '__return_false');
add_filter('auto_update_theme', '__return_false');


include_once 'project-page-fields.php';
include_once 'project-theme-pll.php';
