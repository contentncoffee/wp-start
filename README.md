# Run This
```
bash <(wget -qO- https://bitbucket.org/contentncoffee/wp-start/raw/master/start.sh)
```

## When it is done

- Login into the admin with the following
	- $DATABASE / $DATABASE

*Where $DATABASE is the name of the database that you installed this to.*


- Hide everything from screen options in the dashboard
- Create the languages that you need in the polylang languages.
- Configure the disable comments plugin
- Complete the redirection setup

### Settings General
- Change the site title
- Change the site tagline
- Change Administration Email Address
- Change the date format d/m/Y
- Change the time format H:i

### Create a Home Page

### Delete Sample Pages and Sample Posts

### Settings Reading

- Change Your homepage displays

### Settings Privacy

- Click use this page on the privacy page

### Settings Advanced Editor Tools

- strip down tinymce is all place to the bare minimum need
- disable setting text color
- disable setting background color
- disable editor menu
- remove text color from editor
- remove background color from editor